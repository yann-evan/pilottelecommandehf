﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PiloteTelecommandeHF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initialisation
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            #region récupération des ports COM disponible

            //On efface l'ancienne liste
            COMlist_CB.Items.Clear();

            //On ajoute tout les port COM detecté dans la liste
            foreach (String item in SerialPort.GetPortNames())
            {
                COMlist_CB.Items.Add(item);
            }

            //On affiche par default le premier trouver et on active le bouton Connect
            if (COMlist_CB.Items.Count != 0)
            {
                COMlist_CB.SelectedItem = COMlist_CB.Items[0];
                COMConnect_BT.IsEnabled = true;
            }
            //Si aucun Port COM trouver, on Affiche "No COM" et on desactive le bouton Connect
            else
            {
                COMlist_CB.SelectedItem = "No COM";
                COMConnect_BT.IsEnabled = false;
            }

            #endregion

        }

        /// <summary>
        /// Quand on clique sur la liste de port COM (Combo box) => Rafraichis la liste.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void COMlist_CB_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            //On efface l'ancienne liste
            COMlist_CB.Items.Clear();

            //On ajoute tout les port COM detecté dans la liste
            foreach (String item in SerialPort.GetPortNames())
            {
                COMlist_CB.Items.Add(item);
            }

            //On affiche par default le premier trouver et on active le bouton Connect
            if (COMlist_CB.Items.Count != 0)
            {
                COMlist_CB.SelectedItem = COMlist_CB.Items[0];
                COMConnect_BT.IsEnabled = true;
            }
            //Si aucun Port COM trouver, on Affiche "No COM" et on desactive le bouton Connect
            else
            {
                COMlist_CB.SelectedItem = "No COM";
                COMConnect_BT.IsEnabled = false;
            }
        }
    }
}
